/* eslint-disable linebreak-style */
/* eslint-disable max-len */
/* eslint-disable linebreak-style */
/* eslint-disable eol-last */

const {EmployeeDataViewHandler, addEmployeeHandler, getAllEmployeesHandler, editEmployeeByIdHandler, getEmployeeByIdHandler, deleteEmployeeByIdHandler, indexPage} = require("./handler");

const routes = [
{
  method: 'GET',
  path: '/',
  handler: function (request, response) {
    return response.view('index');
  }
},
{
  method: 'GET',
  path: '/data',
  handler: EmployeeDataViewHandler,
},
{
  method: 'GET',
  path: '/form/data',
  handler: function (request, response) {
    return response.view('data');
  }
},
{
  method: 'POST',
  path: '/form/add',
  handler: addEmployeeHandler,
  options:{
    cors:{
      origin:['*'],
    }
  }
},
{
  method: 'GET',
  path: '/form/get',
  handler: getAllEmployeesHandler
},
{
  method: 'GET',
  path: '/form/get/{formId}',
  handler: getEmployeeByIdHandler,
},
{
  method: 'PUT',
  path: '/form/edit/{formId}',
  handler: editEmployeeByIdHandler
},
{
  method: 'DELETE',
  path: '/form/delete/{formId}',
  handler: deleteEmployeeByIdHandler
}

];

module.exports = routes;