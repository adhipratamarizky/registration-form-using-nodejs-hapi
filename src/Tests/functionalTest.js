const { Builder, By, until } = require("selenium-webdriver");
const assert = require("assert");


async function Test() {
  // launch the browser
  let driver = await new Builder().forBrowser("chrome").build();
  try {

    await driver.get("http://localhost:5000/");
    //Selenium find the html element then input the keys
    await driver.findElement(By.id("name")).sendKeys("EmployeeName");
    await driver.findElement(By.id("email")).sendKeys("newEmployee@abc.com");
    await driver.findElement(By.id("phone")).sendKeys("12345");
    await driver.findElement(By.name("submit")).click();
    
    //On succesful employee data page, get page title
    //Check page title, to confirm add data was successful
    const pageTitle = await driver.getTitle();
    
    // assert usign node assertion
    assert.strictEqual(pageTitle, "Employee Data");
    //Check if redirect to employee data page was successfull
    await driver.wait(until.titleIs("Employee Data"), 4000);

    // Check if the returned data is displayed in a table
  await driver.findElements(By.css('#employee-data tbody tr')).then(async function (tableRows) {
    for (const row of tableRows) {
      const columns = await row.findElements(By.tagName('td'));
      const rowData = await Promise.all(columns.map((column) => column.getText()));
      console.log(rowData.join('\t'));
    }  
      }).catch((error)=>{
        console.error('Error fetching data', error);
      })

  } finally {
    await driver.quit();
  }
}

Test();