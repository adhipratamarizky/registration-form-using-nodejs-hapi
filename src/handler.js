/* eslint-disable linebreak-style */
/* eslint-disable eol-last */

const { response } = require('@hapi/hapi/lib/validation');
const {vision} = require('@hapi/vision');
const { nanoid } = require('nanoid');
const employees = require('./employees');
const Request = require('request');

// Handler for iterate the table of EmployeeDataViewHandler
const viewTableHtmlHandler = (data) => {
  let tableHtml = '<table id="employee-data" border="1"><tr><th>Name</th><th>Email</th><th>Phone</th><th>Updated At</th></tr>';

  //Iterate data json from getAllEmployeesHandler into the html table
  data.employees.forEach((employee) => {
    tableHtml += `<tr>
                    <td>${employee.name}</td>
                    <td>${employee.email}</td>
                    <td>${employee.phone}</td>
                    <td>${employee.updatedAt}</td>
                  </tr>`;
  });

  tableHtml += '</table>';
  return tableHtml;
};

// Handler for showing Employee Data from getAllEmployeesHandler
const EmployeeDataViewHandler = async (request, h) => {
  // Fetch data from the API
  const apiUrl = 'http://localhost:5000/form/get'; //API url of employee form data
  const response = await fetch(apiUrl);
  const data = await response.json();
  
  // Render HTML with fetched data
  try {
    // Render HTML with fetched data in a table
    // const tableHtml = generateTableHtml(data);
    const tableHtml = viewTableHtmlHandler(data);

      return `<html>
                <head><title>Employee Data</title></head>
                <body>
                  <h1>Employees Data:</h1>
                  ${tableHtml}
                </body>
              </html>`;
  } catch (error) {
    console.error('Error fetching data:', error.message);
    return 'Error fetching data.';
  }
}

// Handler for POST new employee data
const addEmployeeHandler = (request, h) => {
  const {
    name, email, phone,
  } = request.payload;

  const id = nanoid(16);
  const updatedAt = new Date().toISOString();
  // const updatedAt = insertedAt;

  if (!name) {
    const response = h.response({
        status: 'fail',
        message: 'Gagal menambahkan registrasi employee. Mohon isi nama employee',
    });

    
    response.code(400);
    return response.redirect('/'); //Route redirect to root
  }
  else if (!email) {
    const response = h.response({
        status: 'fail',
        message: 'Gagal menambahkan registrasi employee. Mohon isi email employee',
    });
    response.code(400);
    return response.redirect('/'); //Route redirect to root
  }
  else if (!phone) {
    const response = h.response({
        status: 'fail',
        message: 'Gagal menambahkan registrasi employee. Mohon isi phone employee',
    });
    response.code(400);
    return response.redirect('/'); //Route redirect to root
  }

  //Adding data employee registration
  const newEmployee = {
    id, name, email, phone, updatedAt,
  };

  employees.push(newEmployee); //Push data to be added in the employees array

  //Validation checking whether the data can be successfully added or not by filtering the employees data using id, name, email, and phone then check the length for each filtered data. If greated than 0 it means the data can be successfully added.
  const checkID = employees.filter((employee) => employee.id === id).length > 0;
  const checkName = employees.filter((employee) => employee.name === name).length > 0;
  const checkEmail = employees.filter((employee) => employee.email === email).length > 0;
  const checkPhone = employees.filter((employee) => employee.phone === phone).length > 0;
  // return checkID;

  if (checkID && checkName && checkEmail && checkPhone) {
    const response = h.response({
      status: 'success',
      message: 'Data berhasil ditambahkan',
      data: {
        employeeId: id,
      },
    });

    response.code(201);
    return response.redirect('/data'); //redirect to front-end showing data tabel (EmployeeDataViewHandler) if the data successfull to be added
  }

  const response = h.response({
    status: 'fail',
    message: 'Data gagal ditambahkan',
  });
  response.code(500);
  return vision.view('index'); //return back to index.html data failed to be added
  
};

// Handler for GET all employee data
const getAllEmployeesHandler = (request, h)=>{
  const response = h.response({
      employees: employees.map((employee) => ({
        id: employee.id,
        name: employee.name,
        email: employee.email,
        phone: employee.phone,
        updatedAt: employee.updatedAt,
    })),
    
  });
  response.code(200);
  return response;
};

// Handler for GET employee data by id
const getEmployeeByIdHandler = (request, h) => {
  const {formId} = request.params;

  const employee = employees.filter((form)=>form.id === formId)[0];

  if(employee !== null){
    return{
      status: 'success',
      data:{
        employee,
      },
    };
  }

  const response = h.response({
    status: 'fail',
    message: 'Data tidak ditemukan',
  });
  response.code(404);
  return response;
}

// Handler for PUT to edit selected employee data by id
const editEmployeeByIdHandler = (request, h)=>{
  const{formId} = request.params;

  const{
    name, email, phone, 
  } = request.payload;


  updatedAt = new Date().toISOString();

  const index = employees.findIndex((employee) => employee.id === formId);

// The findIndex() method returns -1 if no match is found.
  if (index !== -1) {
    if(name !== undefined){

      employees[index] = {
        ...employees[index],
        name,
        updatedAt,
      };
      
    }
    if(email !== undefined){

      employees[index] = {
        ...employees[index],
        email,
        updatedAt,
      };
      
    }
    if(phone !== undefined){

      employees[index] = {
        ...employees[index],
        phone,
        updatedAt,
      };

    }

    const response = h.response({
        status: 'success',
        message: 'Data Registrasi Employee berhasil diperbarui',
      });
      response.code(200);
      return response;
  }
  const response = h.response({
    status: 'fail',
    message: 'Gagal memperbarui data. Id tidak ditemukan',
  });
  response.code(404);
  return response;
}

// Handler for DELETE selected employee data by ID
const deleteEmployeeByIdHandler = (request, h) => {
  const {formId} = request.params;

  const index = employees.findIndex((form) => form.id === formId);

  if (index !== -1) {
    employees.splice(index, 1); //Delete 1 data from position [index]
    const response = h.response({
      status: 'success',
      message: 'Data berhasil dihapus',
    });
    response.code(200);
    return response;
  }

  const response = h.response({
    status: 'fail',
    message: 'Data gagal dihapus. Id tidak ditemukan',
  });
  response.code(404);
  
  return response;
};

module.exports = {EmployeeDataViewHandler, addEmployeeHandler, getAllEmployeesHandler, editEmployeeByIdHandler, getEmployeeByIdHandler, deleteEmployeeByIdHandler};